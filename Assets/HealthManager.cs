﻿using UnityEngine;
using System.Collections;

public class HealthManager : MonoBehaviour {
	public int health,MaxHealth;
	public GameObject Jukensya;
	Vector3 healthScale;
	bool GetFlg;
	void Awake(){
		//MaxHealth=Jukensya.GetComponent<ExamineeStatus>().MAX_LIFE;
		healthScale=gameObject.transform.localScale;

	}
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		Jukensya=GameObject.FindGameObjectWithTag("Juken");

		if(Jukensya.activeInHierarchy&&!GetFlg){
			MaxHealth=Jukensya.GetComponent<ExamineeStatus>().MAX_LIFE;
			GetFlg= true;
		}	

		health=Jukensya.GetComponent<ExamineeStatus>().life;
		gameObject.transform.position=Jukensya.transform.position-Vector3.up+Vector3.right;
		gameObject.transform.parent=Jukensya.transform;
		UpdateHealthBar();

	
	}
	public void UpdateHealthBar ()
	{
		// Set the health bar's colour to proportion of the way between green and red based on the player's health.
		gameObject.renderer.material.color = Color.Lerp(Color.green, Color.red, 1 - health /MaxHealth);
		
		// Set the scale of the health bar to be proportional to the player's health.
		gameObject.transform.localScale = new Vector3(healthScale.x * health /MaxHealth, 1, 1);
	}
}
