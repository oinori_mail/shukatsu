﻿using UnityEngine;
using System.Collections;

public class TimeManger : MonoBehaviour {
	GameObject GameManager;
	void Awake(){
		GameManager=GameObject.Find("GameManager");
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.GetComponent<UILabel>().text=GameManager.GetComponent<RuleManager>().minutes+":"+GameManager.GetComponent<RuleManager>().seconds+"s";
	}

}
