﻿using UnityEngine;
using System.Collections;

public class Debugger : MonoBehaviour {
	string FaceTime="facetime",FacePro="a",HumanHp="b",GameTime="c";
	float FaceTimef,FaceProf,GameTimef;
	int HumanHpi;
	void Awake(){
		FaceTimef=PlayerPrefs.GetFloat("FaceTime");
		FaceProf=PlayerPrefs.GetFloat("FacePro");
		HumanHpi=PlayerPrefs.GetInt("HumanHp");
		GameTimef=PlayerPrefs.GetFloat("GameTime");

		FaceTime=FaceTimef.ToString();
		FacePro=FaceProf.ToString();
		HumanHp=HumanHpi.ToString();
		GameTime=GameTimef.ToString();
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnGUI(){
		GUI.Label(new Rect(Screen.width/2-80,30,50,50),"FaceTime:");
		FaceTime=GUI.TextField(new Rect(Screen.width/2-30,30,50,50),FaceTime);
		GUI.Label(new Rect(Screen.width/2-80,80,50,50),"FacePro:");
		FacePro=GUI.TextField(new Rect(Screen.width/2-30,80,50,50),FacePro);
		GUI.Label(new Rect(Screen.width/2-80,130,50,50),"HumanHp:");
		HumanHp=GUI.TextField(new Rect(Screen.width/2-80,180,50,50),HumanHp);
		GUI.Label(new Rect(Screen.width/2-80, 200,50,50),"GameTime:");
		GameTime=GUI.TextField(new Rect(Screen.width/2-30,180,50,50),GameTime);


		if(GUI.Button(new Rect(Screen.width/2-80,300,50,50),"Ok")){
			FaceProf=float.Parse(FacePro);
			FaceTimef=float.Parse(FaceTime);
			HumanHpi=int.Parse(HumanHp);
			GameTimef=float.Parse(GameTime);
			PlayerPrefs.SetFloat("FaceTime",FaceTimef);
			PlayerPrefs.SetFloat("FacePro",FaceProf);
			PlayerPrefs.SetFloat("GameTime",GameTimef);

			PlayerPrefs.SetInt("HumanHp",HumanHpi);
			Application.LoadLevel("00_Title");
		}


	}

}
