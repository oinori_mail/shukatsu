﻿using UnityEngine;
using System.Collections;

public class InputManager2 : MonoBehaviour {
	private Ray ray;
	private RaycastHit hit;
	public GameObject Jukensya;
	public GameObject bullet;

	public Vector2 Orient;
	Vector2 JukenPosition,MyPosition;
	public float Speed;
	
	private InterviewerRule mensetsu;

	void Awake(){
		Jukensya=GameObject.FindGameObjectWithTag("Juken");
		MyPosition=gameObject.transform.position;
		JukenPosition=Jukensya.transform.position;
		Orient=JukenPosition-MyPosition;
		
		//mensetsu = GetComponentInParent<InterviewerRule>();
		mensetsu = gameObject.transform.parent.GetComponent<InterviewerRule>();
	}
	
	void Update () {

		if(Input.GetMouseButtonDown(0)){
			ray = Camera. main.ScreenPointToRay (Input.mousePosition);
		
			if (Physics.Raycast (ray, out hit, 1000)) {
				GameObject JamaMono;

				if(!mensetsu.IsWatching())
				{
					// 弾のランダム選択
					int rand = Random.Range(1,7);
					switch(rand){
					case 1:
						bullet = (GameObject)Resources.Load("bullet/tama_1");
						break;
					case 2:
						bullet = (GameObject)Resources.Load("bullet/tama_2");
						break;
					case 3:
						bullet = (GameObject)Resources.Load("bullet/tama_3");
						break;
					case 4:
						bullet = (GameObject)Resources.Load("bullet/tama_4");
						break;
					case 5:
						bullet = (GameObject)Resources.Load("bullet/tama_5");
						break;
					case 6:
						bullet = (GameObject)Resources.Load("bullet/tama_6");
						break;
					case 7:
						bullet = (GameObject)Resources.Load("bullet/tama_7");
						break;
					}
					
					JamaMono=Instantiate( bullet ,gameObject.transform.position,gameObject.transform.rotation)as GameObject;
					JamaMono.rigidbody2D.velocity=Orient*Speed;
					
					// タッチにより警戒度上昇
					mensetsu.WatchLevelUp();
					
					//Write here something when be touhched.
				}
				// 面接官に見つかったら死亡
				else
				{
					gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Okoru");
					Destroy(this.gameObject);
					RuleManager.NoticeDeadHeckers();
					GameObject.Find("GameManager").SendMessage("DoBomb",transform.position);
					
				}
			}
		}
	} 
	void InputMouse(){
		if(Input.GetMouseButtonDown(0)){
			ray = Camera. main.ScreenPointToRay (Input.mousePosition);
			
			if (Physics.Raycast (ray, out hit, 1000)) {
				GameObject JamaMono;
				
				if(!mensetsu.IsWatching())
				{
					// 弾のランダム選択
					int rand = Random.Range(1,7);
					switch(rand){
					case 1:
						bullet = (GameObject)Resources.Load("bullet/tama_1");
						break;
					case 2:
						bullet = (GameObject)Resources.Load("bullet/tama_2");
						break;
					case 3:
						bullet = (GameObject)Resources.Load("bullet/tama_3");
						break;
					case 4:
						bullet = (GameObject)Resources.Load("bullet/tama_4");
						break;
					case 5:
						bullet = (GameObject)Resources.Load("bullet/tama_5");
						break;
					case 6:
						bullet = (GameObject)Resources.Load("bullet/tama_6");
						break;
					case 7:
						bullet = (GameObject)Resources.Load("bullet/tama_7");
						break;
					}
					
					JamaMono=Instantiate( bullet ,gameObject.transform.position,gameObject.transform.rotation)as GameObject;
					JamaMono.rigidbody2D.velocity=Orient*Speed;
					
					// タッチにより警戒度上昇
					mensetsu.WatchLevelUp();
					
					//Write here something when be touhched.
				}
				// 面接官に見つかったら死亡
				else
				{
					gameObject.transform.parent.GetComponent<Animator>().SetTrigger("Okoru");
					Destroy(this.gameObject);
					RuleManager.NoticeDeadHeckers();
					GameObject.Find("GameManager").SendMessage("DoBomb",transform.position);
					
				}
			}
		}
	}
}
