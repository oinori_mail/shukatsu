﻿using UnityEngine;
using System.Collections;

public class RuleManager : MonoBehaviour {
	// 制限時間
	public  const float timeLimit = 1*30f;

	// タイマー
	public float timer;


	// 分
	public int minutes;
	// 秒
	public int seconds;

	// 妨害者の数
	private static int heckers = 3;
	// クリアフラグ
	private static bool isClear;
	// ゲームオーバーフラグ
	private static bool isDead;
	int level;
	void Awake(){
		heckers = 3;
		isClear = false;
		isDead = false;
		level=PlayerPrefs.GetInt("Stage");
	}
	
	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("GameTime") > 0){
			if(level==1)
			timer = PlayerPrefs.GetFloat("GameTime");
			if(level==2)
				timer = PlayerPrefs.GetFloat("GameTime")/1.2f;
			if(level==3)
				timer= PlayerPrefs.GetFloat("GameTime")/1.5f;

		}else{
			timer = timeLimit;
		}
	}
	
	// Update is called once per frame
	void Update () {

		// 時間経過所得
		if(timer >= 0 && !isClear)
			timer -= Time.deltaTime;

		//guiText.text = timer.ToString();
		minutes = (int)(timer / 60);
		seconds = (int)(timer % 60);

		// Debug.Log(minutes.ToString() + ":" + seconds.ToString());

		if(timer < 0){
			// ゲームオーバー処理
			isDead = true;
		}
	}

	void OnGUI(){
		GUI.Label(new Rect(Screen.width/2,Screen.height/2,100,100),"dead!"+isDead);
	}

	public static void NoticeDeadHeckers(){
		heckers--;
		if(heckers <= 0){
			isDead = true;
		}
	}

	public static void NoticeDeadExaminee(){
		// TODO ゲームクリア処理
		isClear = true;
	}

	public static bool IsClear(){
		return isClear;
	}

	public static bool IsDead(){
		return isDead;
	}

	public static void Reset(){
		isClear = false;
		isDead = false;
	}

	public void TimeFreeze(){
	if(Time.timeScale==1)
		Time.timeScale=0f;
	if(Time.timeScale==0f)
		Time.timeScale=1;
	}
}
