﻿//when player touched this line,let it destroy and play destroy animation.
using UnityEngine;
using System.Collections;

public class DetectLine : MonoBehaviour {
	Vector2 Orient;
	public GameObject DieAnimation;
	 Quaternion QUp=Quaternion.Euler(0, 0, 0);
	Quaternion QRight=Quaternion.Euler(0, 0, 90);
	Quaternion QLeft=Quaternion.Euler(0, 0, 180);
	Quaternion QDown=Quaternion.Euler(0, 0, 270);
	//the rotation that animation played.

	void Awake(){

	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag=="Juken"||coll.tag=="Bogai"){

			if(gameObject.name=="LeftLine")
				DieAnimation.transform.rotation=QRight;
			if(gameObject.name=="RightLine")
				DieAnimation.transform.rotation=QLeft;
			if(gameObject.name=="UpLine")
				DieAnimation.transform.rotation=QDown;
			if(gameObject.name=="DownLine")
				DieAnimation.transform.rotation=QUp;

			DieAnimation.SetActive(true);

			Destroy(coll.gameObject);
		}
	}
}