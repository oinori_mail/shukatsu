﻿//--------------------------------------------------
// Created on 2015.1.24
// Auther : sakaguchi
// ゲーム実行時にキャラクター、タイルを生成するスクリプト
//--------------------------------------------------
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterGenerate : MonoBehaviour {

	void Start () {
		JukenseGenerate();

		if(PlayerPrefs.GetInt("Stage") > 0){
			CharaGenerate(PlayerPrefs.GetInt("Stage"));
			MapGenerate(PlayerPrefs.GetInt("Stage"));
		}else{
			CharaGenerate(0);
			MapGenerate(0);
		}
	}

	//--------------------------------------------------
	// キャラクター生成
	//--------------------------------------------------
	private float charaOffs = -2;
	private GameObject chara;
	private GameObject chara1;
	private GameObject chara2;
	private GameObject chara3;
	private GameObject chara4;
	private GameObject chara5;
	void CharaGenerate(int stage){
		switch(stage){
		case 1:	// ステージ1
			// 面接官と、面接者のプレハブセットを作成
			chara = (GameObject)Resources.Load( "CharacterSet" );
			chara1 = (GameObject)Resources.Load( "CharacterSet1" );
			chara2 = (GameObject)Resources.Load( "CharacterSet2" );
			chara3 = (GameObject)Resources.Load( "CharacterSet3" );
			chara4 = (GameObject)Resources.Load( "CharacterSet4" );
			chara5 = (GameObject)Resources.Load( "CharacterSet5" );
			break;
		case 2:
			// 面接官と、面接者のプレハブセットを作成
			chara = (GameObject)Resources.Load( "BBA/CharacterSetBaBa1" );
			chara1 = (GameObject)Resources.Load( "BBA/CharacterSetBaBa2" );
			chara2 = (GameObject)Resources.Load( "BBA/CharacterSetBaBa3" );
			chara3 = (GameObject)Resources.Load( "BBA/CharacterSetBaBa4" );
			chara4 = (GameObject)Resources.Load( "BBA/CharacterSetBaBa5" );
			chara5 = (GameObject)Resources.Load( "BBA/CharacterSetBaBa6" );
			break;
		case 3:
			// 面接官と、面接者のプレハブセットを作成
			chara = (GameObject)Resources.Load( "Loli/CharacterSetLoli" );
			chara1 = (GameObject)Resources.Load( "Loli/CharacterSetLoli2" );
			chara2 = (GameObject)Resources.Load( "Loli/CharacterSetLoli3" );
			chara3 = (GameObject)Resources.Load( "Loli/CharacterSetLoli4" );
			chara4 = (GameObject)Resources.Load( "Loli/CharacterSetLoli5" );
			chara5 = (GameObject)Resources.Load( "Loli/CharacterSetLoli6" );
			break;
		default:
			// 面接官と、面接者のプレハブセットを作成
			chara = (GameObject)Resources.Load( "CharacterSet" );
			chara1 = (GameObject)Resources.Load( "CharacterSet1" );
			chara2 = (GameObject)Resources.Load( "CharacterSet2" );
			chara3 = (GameObject)Resources.Load( "CharacterSet3" );
			chara4 = (GameObject)Resources.Load( "CharacterSet4" );
			chara5 = (GameObject)Resources.Load( "CharacterSet5" );
			break;
		}


		List<GameObject> list = new List<GameObject>();
		list.Add( chara );
		list.Add( chara1 );
		list.Add( chara2 );
		list.Add( chara3 );
		list.Add( chara4 );
		list.Add( chara5 );


		for(int i=0;i<3;i++){
			int rand = Random.Range(0,list.Count );
			list[ rand ] = (GameObject)Instantiate( list[ rand ] ,new Vector3( (i * 2) + charaOffs, 0 , 0 ) , Quaternion.identity ); 

			list.Remove(list[ rand ]);
		}
	}


	//--------------------------------------------------
	// 地面のタイル生成
	//--------------------------------------------------
	private GameObject tile;
	void MapGenerate(int stage){
		GameObject tile_material;

		switch(stage){
		case 1:
			tile_material = (GameObject)Resources.Load( "spt_tile" );
			break;
		default:
			tile_material = (GameObject)Resources.Load( "spt_tile" );
			break;
		}
		 
		tile = (GameObject)Instantiate( tile_material,
		                                   new Vector3( 0 , 0 , 0 ) ,
		                                   Quaternion.identity );
	}

	//--------------------------------------------------
	// 受験者生成
	//--------------------------------------------------
	private GameObject jukensei;
	void JukenseGenerate(){
		jukensei = (GameObject)Instantiate( (GameObject)Resources.Load( SelectChara() ) ,
		                                   new Vector3( 0 , 1 , 0 ) ,
		                                   Quaternion.identity );
	}

	//--------------------------------------------------
	// ランダムにキャラを選択
	//--------------------------------------------------
	string SelectChara(){
		int rand = Random.Range(0,2);
		string name;
		if( rand == 0 ){
			name = "spt_jukensha";
		}else{
			name = "spt_jukensha_woman";
		}
		return name;
	}

}
