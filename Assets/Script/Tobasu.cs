﻿using UnityEngine;
using System.Collections;

public class Tobasu : MonoBehaviour {
	public bool LetItGo;
	public GameObject TobasuGameObject;
	public Vector2 Speed;

	void Start () {
		// 若干の時間をおいてから割り当てる。
		Invoke("LoadJukensha" , 1.0f );
	}
	

	void Update () {
		// 吹っ飛ばす準備してる常に
		if(LetItGo){
			TobasuGameObject.rigidbody2D.velocity=Speed;
			ShakeCamera();
			DoBomb( new Vector3(0,0,0) );
		}
	}

	//--------------------------------------------------
	// 受験者を探して割り当て
	//--------------------------------------------------
	private GameObject jukensha;
	void LoadJukensha(){
		if(GameObject.Find("spt_jukensha_woman(Clone)")){
			jukensha = GameObject.Find("spt_jukensha_woman(Clone)");
			TobasuGameObject = jukensha;
			return;
		}
		if(GameObject.Find("spt_jukensha(Clone)")){
			jukensha = GameObject.Find("spt_jukensha(Clone)");
			TobasuGameObject = jukensha;
			return;
		}
	}

	//--------------------------------------------------
	// カメラ格納用変数
	//--------------------------------------------------
	private GameObject camera;
	void ShakeCamera(){
		camera = GameObject.Find("Main Camera");
		camera.transform.position = new Vector3(0,1,-10);
		iTween.ShakePosition(camera, iTween.Hash("x", -2));
		iTween.ShakePosition(camera, iTween.Hash("y", 1));
		return;
	}
	//--------------------------------------------------
	// 爆発エフェクト表示
	// InputManagerからSendMessageを受け取っている。
	//--------------------------------------------------
	public void DoBomb( Vector3 _pos ){
		ShakeCamera();
		GameObject bakuha = (GameObject)Resources.Load( "Animation/die_anime_10003" );
		// ポジションの決定
		Instantiate( bakuha , _pos ,Quaternion.identity );
		return;
	}

}
