﻿using UnityEngine;
using System.Collections;

public class ExamineeStatus : MonoBehaviour {
	public int MAX_LIFE = 10;
	public int life;
	int level;
	public GameObject audios;

	// Use this for initialization
	void Start () {
		audios=GameObject.Find("Explosion");
		level=PlayerPrefs.GetInt("Stage");
		if(PlayerPrefs.GetInt("HumanHp") > 0){
			if(level==1)
			life = PlayerPrefs.GetInt("HumanHp");
			if(level==2)
				life = PlayerPrefs.GetInt("HumanHp")*2;
			if(level==3)
				life = PlayerPrefs.GetInt("HumanHp")*3;


		}else{
			life = MAX_LIFE;
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter2D(Collider2D coll){
		if(coll.tag.Equals("Tama")){
			audios.audio.Play();
			Destroy(coll.gameObject);
			life--;
			if(life < 0){
				Destroy(gameObject);
				RuleManager.NoticeDeadExaminee();
				GameObject.Find("GameManager").SendMessage("DoBomb",transform.position);
			}
		}
	}
}
