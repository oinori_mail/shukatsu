﻿using UnityEngine;
using System.Collections;

// 面接官のルール
public class InterviewerRule : MonoBehaviour {
	// 警戒度
	private float watchLevel;
	
	// 最大警戒度
	public float MAX_WATCH_LEVEL = 3;
	
	// 顔を上げているか
	private bool isWatch;
	
	// 顔を上げている最大時間(固定値)
	public float MAX_WATCH_TIME = 5;
	
	// 顔を上げている時間
	private float watchTime;
	
	// 顔を上げる判定までの最大経過時間
	public float MAX_WAIT_TIME = 3;
	
	// 顔を上げる為の経過
	private float waitTime;

	// 顔を上げる確率
	private float FaceProf = 50;
	
	// アニメーション
	private Animator animator;

	int level;
	// Use this for initialization
	void Start () {
		level=PlayerPrefs.GetInt("Stage");

		if(PlayerPrefs.GetFloat("FaceTime") > 0){
			if(level==1)
			MAX_WATCH_TIME = PlayerPrefs.GetFloat("FaceTime");
			if(level==2)
				MAX_WATCH_TIME = PlayerPrefs.GetFloat("FaceTime")/1.2f;
			if(level==3)
				MAX_WATCH_TIME = PlayerPrefs.GetFloat("FaceTime")/1.5f;

		}
		if(PlayerPrefs.GetFloat("FacePro") > 0){
			if(level==1)
			FaceProf=PlayerPrefs.GetFloat("FacePro");
			if(level==2)
				FaceProf=PlayerPrefs.GetFloat("FacePro")*1.2f;
			if(level==3)
				FaceProf=PlayerPrefs.GetFloat("FacePro")*1.5f;


		}
		watchLevel = 0f;
		isWatch = false;
		watchTime = 0f;
		waitTime = 0f;
		
		animator = GetComponent<Animator>();
		animator.SetTrigger("Sageru");
	}
	
	// Update is called once per frame
	void Update () {
		if(isWatch){
			watchTime += Time.deltaTime;
			if(watchTime >= MAX_WATCH_TIME){
				SetWatch(false);
			}
		}else{
			// 時間経過で顔を上げる
			waitTime += Time.deltaTime;
			if(waitTime >= MAX_WAIT_TIME){
				waitTime = 0f;
				if(Random.Range(0, 100) > FaceProf){
					SetWatch(true);
					watchTime = 0f;
				}
			}
			// 警戒度MAXで顔を上げる
			if(watchLevel >= MAX_WATCH_LEVEL){
				SetWatch(true);
				watchLevel = 0f;
				watchTime = 0f;
			}
		}
		
		if(isWatch){
			animator.SetTrigger("Ageru");
		}else{
			animator.SetTrigger("Sageru");
		}
		// Debug.Log(isWatch.ToString()+":"+waitTime.ToString()+":"+watchTime.ToString());
		
		if(Input.GetKey(KeyCode.Space)){
			watchLevel++;
		}
	}
	
	// 警戒度を上げる
	public void WatchLevelUp(){
		//Debug.Log("noticeWatch");
		watchLevel++;
	}
	
	private void SetWatch(bool flag){
		// 上を向く
		if(flag){
			isWatch = true;
			animator.SetTrigger("Ageru");
		}
		// 下を向く
		else{
			isWatch = false;
			animator.SetTrigger("Sageru");
		}
	}
	
	// 顔を上げているか調べる
	public bool IsWatching(){
		return isWatch;
	}
}
