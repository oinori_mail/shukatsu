﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {
	public GameObject Filter,Title,Saikai,Retry;
	GameObject GameManager;
	int i;
	bool flg;
	//public int i=0;
	void Awake(){
		GameManager=GameObject.Find("GameManager");
		Time.timeScale=1;
		i=0;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//Time.timeScale=0;
		 if(Filter.activeInHierarchy)
			flg=true;
		else
			flg=false;
		if(flg)
			Time.timeScale=0;
		else
			Time.timeScale=1;

	}
	public void PauseFunction(){
		//i=1;
		//GameManager.GetComponent<RuleManager>().TimeFreeze();
		//if(Time.timeScale!=0)
			Time.timeScale=0;
		//else
			//Time.timeScale=1;


		SetTrueOrFalse(Filter);
		SetTrueOrFalse(Title);
		SetTrueOrFalse(Saikai);
		SetTrueOrFalse(Retry);
		//i=0;


	}
	void SetTrueOrFalse(GameObject a){
		if(a.activeInHierarchy)
			a.SetActive(false);
		else
			a.SetActive(true);
	}
}
